
/**
 * Bachelor Thesis
 * Matija Pevec 0036479246
 * Software development for machine-to-machine communication battery powered devices
 *
 * Scenario: Smart Room
 *
 * Required equipment:
 *   Libelium Waspmote
 *   Waspmote Events Sensor Board v2.0
 *   Sensors:
 *     1. Luminosity Sensor              - Socket 1 (polarity doesn't matter)
 *     2. Hall Effect Sensor             - Socket 2 & 3 (single wire -> +3.3V (PIN 3 or 6);
                                                         splitted wire -> Output (PIN 4 and 5))
 *     3. Temperature Sensor (MCP9700A)  - Socket 5 (flat side is faced towards the board)
 *     4. Humidity Sensor 3.3V (808H5V5) - Socket 6 (perforated side is faced towards the board)
 *     5. Presence Sensor (PIR)          - Socket 7 (side with capsule is faced on the opposite side of the board)
 */

#include <stdio.h>
#include <WaspSensorEvent_v20.h>
#include <WaspXBee802.h>
#include <WaspFrame.h>


// Default read timeout value in seconds
#define TEMP 120
#define LIGHT 45
#define HUM 12

/** 
 * Time threshold in seconds. When Waspmote wake up 
 * from deep sleep, it will read peroidic sensors
 * only if their countdown timer is less than THRESHOLD
 */
#define THRESHOLD 10


char RX_ADDRESS[] = "000000000000FFFF";
char WASPMOTE_ID[] = "SmartRoom";

float temp_value;
float light_value;
float hum_value;
float hall_effect_value;

unsigned long tempTimeLeft;
unsigned long lightTimeLeft;
unsigned long humTimeLeft;

char tempFlag;
char lightFlag;
char humFlag;
char pirFlag;
char hallEffectClosedFlag;
char hallEffectOpenedFlag;


char* timeRTCformat;



void setup() {

  USB.ON();
  
  // Turn on the sensor board
  SensorEventv20.ON();
  
  // Turn on the RTC
  RTC.ON();

  // Enable interruptions from the board
  SensorEventv20.attachInt();

  // Setting threshold for PIR sensor on socket 7
  SensorEventv20.setThreshold(SENS_SOCKET7, 1);

  // Setting threshold for Hall Effect sensor on socket 3 (closed)
  SensorEventv20.setThreshold(SENS_SOCKET3, 1.5);

  // Setting threshold for Hall Effect sensor on socket 2 (opened)
  SensorEventv20.setThreshold(SENS_SOCKET2, 1.5);

  /**
   * Boolean variables where is stored 
   * information about whether the sensor
   * should be read or not.
   */
  tempFlag = 1;
  lightFlag = 1;
  humFlag = 1;

  pirFlag = 0;
  hallEffectClosedFlag = 0;
  hallEffectOpenedFlag = 0;


  tempTimeLeft = (unsigned long) TEMP * 1000;
  lightTimeLeft = (unsigned long) LIGHT * 1000;
  humTimeLeft = (unsigned long) HUM * 1000;


  timeRTCformat = reformatTime(calcMinOffsetTime(tempTimeLeft, lightTimeLeft, humTimeLeft));

  frame.setID( WASPMOTE_ID );
  
  // init XBee
  xbee802.ON();
  
  xbee802.wake();
  
  // set sleep mode
  xbee802.setSleepMode(1);


  RTC.setTime("09:06:29:02:00:00:00");
  RTC.getTime();
  USB.println(F("Starting the measurements..."));




}

void loop() { 
  
  
  frame.createFrame(ASCII);
  frame.addSensor(SENSOR_BAT, PWR.getBatteryLevel());
  
  USB.printf("-----------------------------\n");
  USB.printf("Reading sensor values:\n");

  // Check which sensors should be read.

  if(pirFlag) {
    frame.addSensor(SENSOR_PIR, 1);
    USB.println("  PIR activated!!!");
    pirFlag = 0;
  }

  if (hallEffectClosedFlag || hallEffectOpenedFlag) {
    hall_effect_value = SensorEventv20.readValue(SENS_SOCKET3);
    if (hall_effect_value) {
      frame.addSensor(SENSOR_HALL, 1);
      USB.println("  Hall Effect: CLOSED ");
    } else {
      frame.addSensor(SENSOR_HALL, 0);
      USB.println("  Hall Effect: OPENED ");
    }
    hallEffectClosedFlag = 0;
    hallEffectOpenedFlag = 0;

  }

  if(tempFlag) {
    // Print the TEMPERATURE
    temp_value = SensorEventv20.readValue(SENS_SOCKET5, SENS_TEMPERATURE);
    frame.addSensor(SENSOR_TCA, temp_value);
    USB.print("  Temperature: ");
    USB.print(temp_value);
    USB.println(" C");

    tempFlag = 0;
  }

  if(lightFlag) {
    // Print the LIGHT
    light_value = SensorEventv20.readValue(SENS_SOCKET1, SENS_RESISTIVE);  
    frame.addSensor(SENSOR_LUM, light_value);
    USB.print("  Light:       ");
    USB.print(light_value);
    USB.println(" kOhm");

    lightFlag = 0;
  }


  if(humFlag) {
    // Print the HUMIDITY
    hum_value = SensorEventv20.readValue(SENS_SOCKET6, SENS_HUMIDITY);
    frame.addSensor(SENSOR_HUMA, hum_value);
    USB.print("  Humidity:    ");
    USB.print(hum_value);
    USB.println(" \%");

    humFlag = 0;
  }

  USB.printf("-----------------------------\n");

  frame.showFrame();
  //xbee802.send( RX_ADDRESS, frame.buffer, frame.length );
  xbee802.sleep();
  
  USB.printf("Entering into \"Deep Sleep\" mode for %s\n\n", timeRTCformat);
  PWR.deepSleep(timeRTCformat,RTC_OFFSET,RTC_ALM1_MODE1,SOCKET0_OFF);

  USB.ON();
  USB.println(F("Waking up!\n"));
  xbee802.wake(); 




  // Check interruption from Sensor Board
  if(intFlag & SENS_INT) {

    interrupt_function();

    // Clean the interruption flag
    intFlag &= ~(SENS_INT);
  }

  // Check interruption from RTC alarm
  if( intFlag & RTC_INT ) {   
    USB.println(F("-----------------------------"));
    USB.println(F("RTC INT captured"));
    USB.println(F("-----------------------------"));


    // clear flag
    intFlag &= ~(RTC_INT);
  }

  setFlags();

}







void interrupt_function() {  

  // Disable interruptions from the board
  SensorEventv20.detachInt();

  // Load the interruption flag
  SensorEventv20.loadInt();

  // Interruption from PIR Sensor on socket 7    
  if( SensorEventv20.intFlag & SENS_SOCKET7) {
    pirFlag = 1;
  } 
  
  if( SensorEventv20.intFlag & SENS_SOCKET3) {
    // Interruption from Hall Effect Sensor on socket 1
    hallEffectClosedFlag = 1;
  } 
  
  if( SensorEventv20.intFlag & SENS_SOCKET2) {
    // Interruption from Hall Effect Sensor on socket 1
    hallEffectOpenedFlag = 1;
  }

  // Clean the interruption flag
  intFlag &= ~(SENS_INT);

  // Enable interruptions from the board
  SensorEventv20.attachInt();

}




// Reformats time in the RTC format. Supports only
// time that is no longer than 1 day ( only hours, 
// minutes and seconds). The time format that function
// returns will look like this: "00:01:50:10\0"
char* reformatTime(unsigned long timeInMilis) {
  unsigned long cutTime;
  int hours;
  int minutes;
  int seconds;

  char* text = (char *) malloc(13);

  cutTime = timeInMilis;

  // cutting miliseconds
  cutTime /= 1000;

  // getting seconds
  seconds = cutTime % 60;
  cutTime /= 60;

  // getting minutes
  minutes = cutTime % 60;
  cutTime /= 60;

  // getting hours
  hours = cutTime % 60;

  sprintf(text, "00:%.2d:%.2d:%.2d\0", hours, minutes, seconds);
  return text;
}

/**
 * Calculates the shortest time offset of 3 values
 * returns value in milliseconds
 */
unsigned long calcMinOffsetTime(unsigned long val1, unsigned long val2, unsigned long val3) {

  unsigned long minValue = 10000000;

  minValue = val1 < minValue && val1 >= (unsigned long)THRESHOLD*1000 ? val1 : minValue;
  minValue = val2 < minValue && val2 >= (unsigned long)THRESHOLD*1000 ? val2 : minValue;
  minValue = val3 < minValue && val3 >= (unsigned long)THRESHOLD*1000 ? val3 : minValue;

  return minValue;
}


void setFlags() {

  // the time passed between two sensor reading cycles
  unsigned long timePassed = timePassedInMillis();


  if (timePassed > tempTimeLeft || tempTimeLeft - timePassed < (unsigned long) THRESHOLD * 1000) {
    tempFlag = 1;
    tempTimeLeft = (unsigned long) TEMP * 1000;
  } else {
    tempTimeLeft -= timePassed;
  }

  if (timePassed > lightTimeLeft || lightTimeLeft - timePassed < (unsigned long) THRESHOLD * 1000) {
    lightFlag = 1;
    lightTimeLeft = (unsigned long) LIGHT * 1000;
  } else {
    lightTimeLeft -= timePassed;
  }

  if (timePassed > humTimeLeft || humTimeLeft - timePassed < (unsigned long) THRESHOLD * 1000) {
    humFlag = 1;
    humTimeLeft = (unsigned long) HUM * 1000;
  } else {
    humTimeLeft -= timePassed;
  }

  timeRTCformat = reformatTime(calcMinOffsetTime(tempTimeLeft, lightTimeLeft, humTimeLeft));

  USB.println("Sensor schedule:");
  USB.printf("  Temperature: %d s  ", tempTimeLeft/1000);
  USB.println(tempTimeLeft == (unsigned long) TEMP * 1000 ? "(RESET!)" : "");
  USB.printf("  Light:       %d s  ", lightTimeLeft/1000);
  USB.println(lightTimeLeft == (unsigned long) LIGHT * 1000 ? "(RESET!)" : "");
  USB.printf("  Humidity:    %d s  ", humTimeLeft/1000);
  USB.println(humTimeLeft == (unsigned long) HUM * 1000 ? "(RESET!)" : "");
} 



unsigned long timePassedInMillis() {
  RTC.getTime();
  unsigned long timeInMillis = ((unsigned long)RTC.second + 60 * (unsigned long)RTC.minute + 3600 * (unsigned long)RTC.hour ) * 1000;
  RTC.setTime("09:06:29:02:00:00:00");
  return timeInMillis;
}















